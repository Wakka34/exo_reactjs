import React, { Component } from 'react';
import './App.css';
import {Container, Row, Col} from 'react-bootstrap';
import Header from './component/header/header'
import Favorite from './component/favorite/favorite'
import Searchbar from './component/searchbar/searchbar'
import List from './component/list/list'
import Footer from './component/footer/footer'


class App extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      arrayFilm:[]
    }

    this.callbackArrayFilm = this.callbackArrayFilm.bind(this);

  }
  
  callbackArrayFilm(films) {
    this.setState({arrayFilm:films})
    console.log("apps",films)
  }

  render() {
    return (
     <Container fluid={true}>
       <Header></Header>
       <Row>
         <Col sm={3}>
          <Favorite></Favorite>
         </Col>
         <Col sm={9}>
           <Searchbar  callbackFromParent={this.callbackArrayFilm}></Searchbar>
           <List data={this.state.arrayFilm.results}></List>
         </Col>
       </Row>
       <Footer></Footer>
     </Container>
    );
  }
}

export default App;
