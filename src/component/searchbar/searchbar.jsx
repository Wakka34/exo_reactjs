import React from 'react';
import { Component } from 'react'
import { InputGroup, FormControl } from 'react-bootstrap';
import imdb from 'imdb-api'



class Searchbar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      searchTerm:''
    }

    this.handleChange = this.handleChange.bind(this);

  }

  handleChange(event) {
    this.setState({searchTerm: event.target.value});
    console.log(event.target.value)
    fetch('https://api.themoviedb.org/3/search/movie?api_key=a5842037c731d7032cb2525fbd96ec26&language=fr-FR&query=' + this.state.searchTerm  + '&page=1&include_adult=false')
    .then(res => res.json())
    .then( res => {
      console.log(res)
      this.props.callbackFromParent(res);

    })
  }

  render() {
    return <div>
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <InputGroup.Text>Recherche</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl value={this.state.value} onChange={this.handleChange}
          placeholder="Saisir le terme à rechercher"

        />
      </InputGroup>
    </div>;
  }
}

export default Searchbar