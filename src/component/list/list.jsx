import React from 'react';
import { Component } from 'react'


class List extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }


    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data !== this.state.data) {
            this.setState({ data: nextProps.data});
        }
        console.log('toto')
    }
    render() {
        let renderFilms = ''
        if (this.state.data !== undefined && this.state.data.length !== 0 ) {
            renderFilms = this.state.data.map((film) => {
                return <div>
                    <h4>{film.title}</h4>
                </div>
            })
        }
        return <div>
            {renderFilms}
        </div>;
    }
}

export default List